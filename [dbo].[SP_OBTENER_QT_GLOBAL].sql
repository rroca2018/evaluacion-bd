USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_OBTENER_QT_GLOBAL]    Script Date: 30/08/2020 16:58:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_OBTENER_QT_GLOBAL]
	@ID_EMPRESA INT,
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT
AS

DECLARE @COUNT INT;
BEGIN

		SELECT Qt_Respuestas AS QTPREGUNTASGLOBAL
		FROM BAL_EMPRESA
		WHERE BAL_EMPRESA.CO_EMPRESA = @ID_EMPRESA
		AND BAL_EMPRESA.Co_Estado = 1


		SELECT @COUNT = @@ROWCOUNT
			
		IF @COUNT > 0
		BEGIN
			SELECT @COD_RESULTADO = 1;
			SELECT @MSG_RESULTADO = 'OK';
		END
		ELSE
		BEGIN 
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO =  'No se encotraron datos!';
		END
END
