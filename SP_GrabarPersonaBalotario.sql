
-- =============================================
-- Author:		Ronald Roca
-- Description:	Crear o actualizar en tabla BAL_BALOTARIO_PERSONA
-- =============================================
CREATE PROCEDURE [dbo].[SP_GrabarPersonaBalotario]
	@Co_Balotario			int,
	@Co_Usuario				int,
	@flagEditar				int,
	@COD_RESULTADO			INT OUTPUT,
	@MSG_RESULTADO			VARCHAR(3000) OUTPUT
AS
SET NOCOUNT ON;

-- SELECT * FROM BAL_BALOTARIO_PERSONA

	if @flagEditar = 1
	BEGIN
--		Elimina registros.
		DELETE FROM BAL_BALOTARIO_PERSONA
		WHERE Co_Balotario = @Co_Balotario;	
	END
		
	if @flagEditar = 0 OR @flagEditar = 1
	BEGIN
		INSERT INTO BAL_BALOTARIO_PERSONA
		(Co_Balotario,
		Co_Usuario)
		VALUES
		(@Co_Balotario,
		@Co_Usuario)

		SELECT @COD_RESULTADO = 1;
		SELECT @MSG_RESULTADO = 'OK';
	END;