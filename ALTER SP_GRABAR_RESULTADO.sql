USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_GRABAR_RESULTADO]    Script Date: 26/08/2020 18:38:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_GRABAR_RESULTADO]
	@CoEmpresa			INT,
	@CoBalotario		INT,
	@CoUsuario			INT,
	@QtIntentos			INT,
	@QtNota				DECIMAL(19,2),
	@COD_RESULTADO		INT OUTPUT,
	@MSG_RESULTADO		VARCHAR(3000) OUTPUT
AS

SET NOCOUNT ON;
BEGIN TRY

	INSERT INTO BAL_RESULTADO(Co_empresa, Co_Balotario, Qt_Intento, Qt_Nota, Co_Usuario) 
	VALUES (@CoEmpresa, @CoBalotario, @QtIntentos, @QtNota, @CoUsuario)

	SELECT @COD_RESULTADO = 1;
	SELECT @MSG_RESULTADO = 'OK';
		
END TRY
BEGIN CATCH
	THROW;
END CATCH



