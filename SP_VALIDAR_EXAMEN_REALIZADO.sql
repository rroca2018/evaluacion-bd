USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_VALIDAR_EXAMEN_REALIZADO]    Script Date: 15/07/2020 22:07:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joel Reymundez
-- Create date: 14/07/2020
-- Description:	Validar si el usuario ya ha realizado su examen
-- Modificado date: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_VALIDAR_EXAMEN_REALIZADO]
	
	@COD_USUARIO INT,
	@ID_BALOTARIO BIGINT=0,
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT
	
AS

DECLARE @ROWCOUNT INT;


		SELECT  BAL_RESULTADO.CO_USUARIO FROM BAL_RESULTADO, BAL_BALOTARIO
		WHERE BAL_RESULTADO.CO_BALOTARIO = @ID_BALOTARIO
		AND BAL_RESULTADO.CO_USUARIO = @COD_USUARIO
	 
		SELECT @ROWCOUNT = @@ROWCOUNT
		IF(@ROWCOUNT > 0)
		BEGIN
			SELECT @COD_RESULTADO = 1;
			SELECT @MSG_RESULTADO = 'OK';
		END

		ELSE
		BEGIN
			SELECT @COD_RESULTADO = -1;
			SELECT @MSG_RESULTADO = 'NO';
		END

