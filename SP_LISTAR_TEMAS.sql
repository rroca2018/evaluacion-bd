USE [BD_BALOTARIO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Joel Reymundez
-- Create date: 02/07/2020
-- Description:	Obtiene la lista de temas que existe
-- Modificado date: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_LISTAR_TEMAS]

	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT
AS

	BEGIN TRANSACTION
	BEGIN TRY

		SELECT 
				[Co_empresa] 
			  ,[Co_Tema] AS CODIGO
			  ,[No_DescTema] AS TEMA
			  ,[No_DescCortaTema]
			  ,[Co_Estado]
		FROM BAL_TEMA
		COMMIT TRANSACTION


		SELECT @COD_RESULTADO = 1;
		SELECT @MSG_RESULTADO = 'OK';

		
	END TRY
	BEGIN CATCH
		
		ROLLBACK TRANSACTION

		SELECT @COD_RESULTADO = 0;
		SELECT @MSG_RESULTADO = ERROR_MESSAGE();

	END CATCH
	

