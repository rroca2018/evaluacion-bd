USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[buscarDato]    Script Date: 03/09/2020 21:29:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[buscarDato]
	@DATO varchar(20),
	@VALOR varchar(160),
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT

AS
	DECLARE @rowcount INT;

	IF @DATO = 'EMPRESA'
	BEGIN
		SELECT  Co_empresa
		FROM BAL_EMPRESA WHERE No_NombreEmpresa = @VALOR

		SELECT @rowcount = @@ROWCOUNT
		
		IF @rowcount > 0
			BEGIN
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';
			END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'Empresa no existe';
		END

	END

	ELSE IF @DATO = 'USUARIO'
	BEGIN 
		SELECT  Co_Usuario 
		FROM BAL_USUARIO WHERE No_Usuario = @VALOR 
			
		SELECT @rowcount = @@ROWCOUNT

		IF @rowcount > 0
			BEGIN
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';
				
			END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'USUARIO NO EXISTE';
		END
	END


	ELSE IF @DATO = 'TIPODOCUMENTO'
	BEGIN 
		SELECT Co_TipoDocumento 
		FROM BAL_TIPODOCU WHERE No_DescTipoDocu = @VALOR 
		AND Co_Estado = 1
			
		SELECT @rowcount = @@ROWCOUNT

		IF @rowcount > 0
			BEGIN
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';
			END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'Tipo de documento no existe';
		END
	END

	ELSE IF @DATO = 'CARGO'
	BEGIN 
		SELECT Co_Cargo
		FROM BAL_CARGO  WHERE No_DescCargo = @VALOR 
		AND Co_Estado = 1
			
		SELECT @rowcount = @@ROWCOUNT

		IF @rowcount > 0
			BEGIN
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';
			END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'Cargo no existe';
		END
	END

	ELSE IF @DATO = 'CATEGORIA'
	BEGIN 
		SELECT CAT.Co_Categoria
		FROM BAL_CATEGORIA CAT, BAL_EMPRESA EMP  WHERE CAT.No_DescCategoria  = @VALOR 
		AND CAT.Co_empresa = EMP.Co_empresa
		AND CAT.Co_Estado = 1
			
		SELECT @rowcount = @@ROWCOUNT

		IF @rowcount > 0
			BEGIN
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';
			END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'Categoria no existe';
		END
	END


	ELSE IF @DATO = 'AREA'
	BEGIN 
		SELECT Co_Area
		FROM BAL_AREA A, BAL_CATEGORIA CAT,  BAL_EMPRESA EMP WHERE No_DescArea = @VALOR 
		AND A.Co_Categoria = CAT.Co_Categoria
		AND A.Co_empresa = EMP.Co_empresa
		AND A.Co_Estado = 1
			
		SELECT @rowcount = @@ROWCOUNT

		IF @rowcount > 0
			BEGIN
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';
			END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'Area no existe';
		END
	END

	ELSE IF @DATO = 'LOCAL'
	BEGIN 
		SELECT L.Co_Local
		FROM BAL_LOCAL L, BAL_CATEGORIA CAT, BAL_AREA A, BAL_EMPRESA EMP WHERE No_DescLocal = @VALOR 
		AND L.Co_Categoria = CAT.Co_Categoria
		AND L.Co_Area = A.Co_Area
		AND L.Co_empresa = EMP.Co_empresa
		AND L.Co_Estado = 1
			
		SELECT @rowcount = @@ROWCOUNT

		IF @rowcount > 0
			BEGIN
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';
			END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'Local no existe';
		END
	END


	ELSE IF @DATO = 'ROL'
	BEGIN 
		SELECT Co_Rol
		FROM BAL_ROL WHERE No_DescRol = @VALOR
		AND Co_Estado = 1
			
		SELECT @rowcount = @@ROWCOUNT

		IF @rowcount > 0
			BEGIN
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';
			END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'Rol no existe';
		END
	END

	ELSE IF @DATO = 'ESTADO'
	BEGIN 
		SELECT Co_Estado
		FROM BAL_ESTADO WHERE No_DescEstado = @VALOR
		
			
		SELECT @rowcount = @@ROWCOUNT

		IF @rowcount > 0
			BEGIN
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';
			END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'Estado no existe';
		END
	END

	ELSE IF @DATO = 'TEMA'
	BEGIN 
		SELECT Co_Tema 
		FROM BAL_TEMA WHERE No_DescTema = @VALOR 
		AND Co_Estado = 1
			
		SELECT @rowcount = @@ROWCOUNT

		IF @rowcount > 0
			BEGIN
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';
			END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'Tema no existe';
		END
	END

		

	






