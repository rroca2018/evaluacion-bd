USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_GrabarBalotario]    Script Date: 11/07/2020 21:33:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ronald Roca
-- Description:	Crear o actualizar en tabla BAL_BALOTARIO 
-- =============================================
ALTER PROCEDURE [dbo].[SP_GrabarBalotario]
	@Co_Balotario			int,
	@Co_Empresa				int,
	@FechaInicio			VARCHAR(20),
	@FechaFin				VARCHAR(20),
	@Descripcion			VARCHAR(100),
	@NroIntentos			int,
	@LimiteTiempo			dec(10,2),
	@ResultadoAprobatorio	dec(10,2),
	@MensajeInicial			varchar(300),
	@TipoBalotario			char(1),
	@co_Cargo				int,
	@co_Categoria			int,
	@co_Area				int,
	@co_Local				int,
	@co_Estado				char(2),
	@flagEditar				int,
	@NEW_IDENTITY			INT = NULL OUTPUT,
	@COD_RESULTADO			INT OUTPUT,
	@MSG_RESULTADO			VARCHAR(3000) OUTPUT
AS
SET NOCOUNT ON;


	if @flagEditar = 0
	BEGIN
	INSERT INTO BAL_BALOTARIO(
	Co_empresa, 
	Fe_Inicio, 
	Fe_Fin, 
	Qt_Intentos,
	Qt_Tiempo,
	No_Mensaje, 
	Co_Cargo, 
	Co_Estado, 
	Fi_TipoBal, 
	No_Descripcion, 
	Co_Categoria,
	Co_Area,
	Co_Local, 
	Qt_CaliAprob) 
	VALUES (
	@Co_Empresa,
	CONVERT(DATE,@FechaInicio, 120),
	CONVERT(DATE,@FechaFin, 120),
	@NroIntentos,
	@LimiteTiempo,
	@MensajeInicial,
	@co_Cargo,
	@co_Estado,
	@TipoBalotario,
	@Descripcion,
	@co_Categoria,
	@co_Area,
	@co_Local,
	@ResultadoAprobatorio
	)
	SET @NEW_IDENTITY = @@IDENTITY;
	SELECT @COD_RESULTADO = 1;
	SELECT @MSG_RESULTADO = 'OK';
	END
	ELSE 

	BEGIN
	
		UPDATE  BAL_BALOTARIO
		SET 
		Fe_Inicio		= CONVERT(DATE,@FechaInicio, 120),
		Fe_Fin			= CONVERT(DATE,@FechaFin, 120),
		Qt_Intentos		= @NroIntentos,
		Qt_Tiempo		= @LimiteTiempo,
		No_Mensaje		= @MensajeInicial,
		Co_Cargo		= @co_Cargo,
		Co_Estado		= @co_Estado,
		Fi_TipoBal		= @TipoBalotario,
		No_Descripcion	= @Descripcion,
		Co_Categoria	= @co_Categoria,
		Co_Area			= @co_Area,
		Co_Local		= @co_Local,
		Qt_CaliAprob	= @ResultadoAprobatorio
		WHERE Co_Balotario = @Co_Balotario

	SELECT @COD_RESULTADO = 1;
	SELECT @MSG_RESULTADO = 'OK';
	END;

