USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_GRABAR_CATEGORIA]    Script Date: 21/09/2020 21:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GRABAR_CATEGORIA]
@Co_Categoria int,
@Desc_Categoria VARCHAR(200),
@Co_Estado int,
@flagEditar int,
@NEW_IDENTITY  INT = NULL OUTPUT,
@COD_RESULTADO INT OUTPUT,
@MSG_RESULTADO VARCHAR(3000) OUTPUT
AS

SET NOCOUNT ON;
BEGIN TRY

if @flagEditar = 0
BEGIN
DECLARE @CODEMPRESA  INT = 1
DECLARE @CODCATEGORIA INT = (SELECT MAX(Co_Categoria)+1 Co_Categoria
FROM BAL_CATEGORIA);
INSERT INTO BAL_CATEGORIA(
Co_Categoria,
Co_empresa,
No_DescCategoria,
No_DescCortaCategoria,
Co_Estado)
VALUES (
@CODCATEGORIA,
@CODEMPRESA,
@Desc_Categoria,
'',
'1'
)
SET @NEW_IDENTITY = @CODCATEGORIA;
SELECT @COD_RESULTADO = 1;
SELECT @MSG_RESULTADO = 'OK';
END

ELSE
BEGIN

UPDATE  BAL_CATEGORIA
SET
No_DescCategoria = @Desc_Categoria,
Co_Estado = @Co_Estado
WHERE Co_Categoria = @Co_Categoria

SELECT @COD_RESULTADO = 1;
SELECT @MSG_RESULTADO = 'OK';
END;
END TRY
BEGIN CATCH

THROW;

END CATCH


