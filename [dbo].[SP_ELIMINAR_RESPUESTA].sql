USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_ELIMINAR_RESPUESTA]    Script Date: 30/08/2020 17:00:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_ELIMINAR_RESPUESTA]
	@COD_PREGUNTA			int,
	@COD_RESULTADO			INT OUTPUT,
	@MSG_RESULTADO			VARCHAR(3000) OUTPUT
AS
SET NOCOUNT ON;

-- SELECT * FROM BAL_BALOTARIO_PERSONA

	
	BEGIN
--		Elimina registros.
		DELETE FROM BAL_RESPUESTA
		WHERE Co_Pregunta = @COD_PREGUNTA;	

		SELECT @COD_RESULTADO = 1;
		SELECT @MSG_RESULTADO = 'OK';
	END
		


