-- =============================================
-- Author:		Ronald Roca
-- Description:	Obtener resultado de la tabla BAL_BALOTARIO_PREGUNTA
-- =============================================
-- EXEC SP_ObtenerPreguntasBalotario 1

CREATE PROCEDURE [dbo].[SP_ObtenerPreguntasBalotario]
	@COD_BALOTARIO INT,
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT

AS
	
	BEGIN TRANSACTION
	BEGIN TRY


		SELECT	--ROW_NUMBER() as correlativo, 
		ROW_NUMBER() OVER(ORDER BY C.No_DescTema ASC) AS correlativo,
		C.No_DescTema, b.No_DescPregunta
		FROM BAL_BALOTARIO_PREGUNTA as A, BAL_PREGUNTA as B, BAL_TEMA as C
		WHERE A.Co_Balotario = @COD_BALOTARIO AND
			  A.Co_Pregunta  = B.Co_Pregunta  AND
			  B.Co_Tema      = C.Co_Tema
		--GROUP BY C.No_DescTema, b.No_DescPregunta
		ORDER BY 2 asc, 3 asc	   

		
		COMMIT TRANSACTION
		
		SELECT @COD_RESULTADO = 1;
		SELECT @MSG_RESULTADO = 'OK';

		
	END TRY
	BEGIN CATCH
		
		ROLLBACK TRANSACTION

		SELECT @COD_RESULTADO = 0;
		SELECT @MSG_RESULTADO = ERROR_MESSAGE();

	END CATCH