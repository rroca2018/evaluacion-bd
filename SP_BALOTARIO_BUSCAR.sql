USE [BD_BALOTARIO]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joel Reymundez
-- Create date: 04/07/2020
-- Description:	Realizar busqueda de balotario por su descripcion
-- Modificado date: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_BALOTARIO_BUSCAR]
	@DESCRIPCION VARCHAR(60),
	
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT

AS

	DECLARE @GLOBAL_COD_EMPRESA INT = 1;

	
	BEGIN TRANSACTION
	BEGIN TRY
		
		SELECT B.Co_Balotario AS CODIGO, B.NO_DESCRIPCION, B.FE_INICIO, B.FE_FIN, B.FI_TIPOBAL, 
		CASE B.FI_TIPOBAL WHEN 'C' THEN 'CARGO' WHEN 'L' THEN 'LOCAL' WHEN 'I' THEN 
		'PERSONAS' END AS TIPO, 
		CASE WHEN (E.No_DescEstado = 'ACTIVO' AND B.Fe_Fin<GETDATE ( )) THEN 'FINALIZADO'
		 ELSE (E.No_DescEstado)
		 END AS ESTADO
		,
		B.CO_ESTADO
		FROM BAL_BALOTARIO B 
		LEFT JOIN BAL_ESTADO E ON B.Co_Estado = E.Co_Estado
		WHERE  (No_Descripcion LIKE '%'+@DESCRIPCION+'%')
		AND Co_empresa = 1

		COMMIT TRANSACTION
		
		SELECT @COD_RESULTADO = 1;
		SELECT @MSG_RESULTADO = 'OK';

		
	END TRY
	BEGIN CATCH
		
		ROLLBACK TRANSACTION

		SELECT @COD_RESULTADO = 0;
		SELECT @MSG_RESULTADO = ERROR_MESSAGE();

	END CATCH
	









