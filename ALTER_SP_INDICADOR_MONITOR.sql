-- EXEC SP_INDICADOR_MONITOR 1, 1, 'A'
-- SELECT * FROM BAL_BALOTARIO_PERSONA
-- sp_helptext SP_INDICADOR_MONITOR
ALTER PROCEDURE [dbo].[SP_INDICADOR_MONITOR]
	@ID_EMPRESA INT=0,
	@ID_BALOTARIO BIGINT=0,
	@ID_TIPOCON CHAR(1)='C',
	@ID_CATEGORIA BIGINT=0,
	@ID_AREA  BIGINT=0,
	@ID_LOCAL BIGINT=0,
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT
AS

DECLARE @COUNT     INT,
		@ROWCOUNT  INT,
		@L_CARGO   INT,
		@L_CARGO_TMP   INT=0,
		@L_AREA   INT,
		@L_AREA_TMP   INT=0,
		@L_LOCAL   INT,
		@L_LOCAL_TMP   INT=0,
		@L_CATEGORIA   INT,
		@L_CATEGORIA_TMP   INT=0,
		@L_FLAG    INT,
		@FECHA_FIN DATE,
		@RESUL	   INT, 
		@CONT      INT,
		@VERDE	   INT=0,
		@TOTAL	   INT=0,
		@ROJO      INT=0,
		@POR_VERDE float,
		@POR_ROJO  float,
		@TIPO_BAL  CHAR(1);
BEGIN

	-- Tipo de evaluación	
	SELECT @TIPO_BAL = FI_TIPOBAL,
		   @FECHA_FIN = FE_FIN	 
	FROM BAL_BALOTARIO 
	WHERE CO_EMPRESA = @ID_EMPRESA
	AND   Co_Balotario = @ID_BALOTARIO

	SELECT @ROWCOUNT = @@ROWCOUNT
	IF(@ROWCOUNT > 0)
	BEGIN
		SELECT @COD_RESULTADO = 1;
		SELECT @MSG_RESULTADO = 'OK';

		IF @ID_TIPOCON = 'C' --- PESTAÑA CARGO
		BEGIN
			IF @TIPO_BAL = 'C' -- x cargo
			BEGIN
				DECLARE C_Datos CURSOR FOR
				SELECT a.CO_CARGO , COUNT(*)
				FROM BAL_USUARIO A, BAL_BALOTARIO B
				WHERE B.CO_EMPRESA = @ID_EMPRESA AND
					  B.CO_BALOTARIO = @ID_BALOTARIO AND
					  A.CO_EMPRESA = B.CO_EMPRESA AND
					  A.CO_CARGO = B.CO_CARGO
				GROUP BY A.CO_CARGO	   	   
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_CARGO, @CONT --,  @L_FLAG
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_CARGO <> @L_CARGO_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_CARGO_TMP = @L_CARGO
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_RESULTADO C
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							  B.CO_BALOTARIO = @ID_BALOTARIO AND
							  A.CO_EMPRESA = B.CO_EMPRESA AND
							  A.CO_CARGO = B.CO_CARGO AND
							  A.CO_CARGO = @L_CARGO AND
							  C.CO_EMPRESA = B.CO_EMPRESA AND
							  C.CO_BALOTARIO = B.CO_BALOTARIO AND
							  C.CO_USUARIO = A.CO_USUARIO AND
							  C.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_CARGO, @CONT -- @L_FLAG
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos
			END

			IF @TIPO_BAL = 'L' -- Local
			BEGIN
				DECLARE C_Datos CURSOR FOR
					SELECT a.CO_CARGO, COUNT(*)
					FROM BAL_USUARIO A, BAL_BALOTARIO B
					WHERE B.CO_EMPRESA = @ID_EMPRESA AND
						  B.CO_BALOTARIO = @ID_BALOTARIO AND
						  A.CO_EMPRESA = B.CO_EMPRESA AND
						  A.CO_LOCAL = B.CO_LOCAL
					GROUP BY A.CO_CARGO	 
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_CARGO, @CONT
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_CARGO <> @L_CARGO_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_CARGO_TMP = @L_CARGO
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_RESULTADO C
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							  B.CO_BALOTARIO = @ID_BALOTARIO AND
							  A.CO_EMPRESA = B.CO_EMPRESA AND
							  A.CO_LOCAL = B.CO_LOCAL AND
							  A.CO_CARGO = @L_CARGO AND
							  C.CO_EMPRESA = B.CO_EMPRESA AND
							  C.CO_BALOTARIO = B.CO_BALOTARIO AND
							  C.CO_USUARIO = A.CO_USUARIO AND
							  C.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_CARGO, @CONT
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos

			END
			
			IF @TIPO_BAL = 'I'  --- Individual
			BEGIN
				DECLARE C_Datos CURSOR FOR
					SELECT a.co_CARGO , COUNT(*)
					FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_BALOTARIO_PERSONA C
					WHERE B.CO_EMPRESA = @ID_EMPRESA AND
						  B.CO_BALOTARIO = @ID_BALOTARIO AND
						  C.CO_BALOTARIO = B.CO_BALOTARIO AND
						  C.CO_USUARIO = A.CO_USUARIO AND
						  A.CO_EMPRESA = B.CO_EMPRESA 
					GROUP BY A.CO_CARGO
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_CARGO, @CONT
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_CARGO <> @L_CARGO_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_CARGO_TMP = @L_CARGO
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_BALOTARIO_PERSONA C, BAL_RESULTADO D
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							B.CO_BALOTARIO = @ID_BALOTARIO AND
							C.CO_BALOTARIO = B.CO_BALOTARIO AND
							C.CO_USUARIO = A.CO_USUARIO AND
							A.CO_EMPRESA = B.CO_EMPRESA AND
							D.CO_EMPRESA = B.CO_EMPRESA AND
							D.CO_BALOTARIO = B.CO_BALOTARIO AND
							D.CO_USUARIO = A.CO_USUARIO AND
							A.CO_CARGO = @L_CARGO AND
						    D.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_CARGO, @CONT
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos
			END
		END   ---- Fin pestaña Cargo

		IF @ID_TIPOCON = 'G' --- PESTAÑA CATEGORIA
		BEGIN
			IF @TIPO_BAL = 'C' -- x cargo
			BEGIN
				DECLARE C_Datos CURSOR FOR
				SELECT a.CO_CATEGORIA , COUNT(*)
				FROM BAL_USUARIO A, BAL_BALOTARIO B
				WHERE B.CO_EMPRESA = @ID_EMPRESA AND
					  B.CO_BALOTARIO = @ID_BALOTARIO AND
					  A.CO_EMPRESA = B.CO_EMPRESA AND
					  A.CO_CARGO = B.CO_CARGO
				GROUP BY A.CO_CATEGORIA	   	   
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_CATEGORIA, @CONT --,  @L_FLAG
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_CATEGORIA <> @L_CATEGORIA_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_CATEGORIA_TMP = @L_CATEGORIA
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_RESULTADO C
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							  B.CO_BALOTARIO = @ID_BALOTARIO AND
							  A.CO_EMPRESA = B.CO_EMPRESA AND
							  A.CO_CARGO = B.CO_CARGO AND
							  A.CO_CATEGORIA = @L_CATEGORIA AND
							  C.CO_EMPRESA = B.CO_EMPRESA AND
							  C.CO_BALOTARIO = B.CO_BALOTARIO AND
							  C.CO_USUARIO = A.CO_USUARIO AND
							  C.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_CATEGORIA, @CONT 
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos
			END

			IF @TIPO_BAL = 'L' -- Local
			BEGIN
				DECLARE C_Datos CURSOR FOR
					SELECT a.CO_CATEGORIA, COUNT(*)
					FROM BAL_USUARIO A, BAL_BALOTARIO B
					WHERE B.CO_EMPRESA = @ID_EMPRESA AND
						  B.CO_BALOTARIO = @ID_BALOTARIO AND
						  A.CO_EMPRESA = B.CO_EMPRESA AND
						  A.CO_LOCAL = B.CO_LOCAL
					GROUP BY A.CO_CATEGORIA	 
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_CATEGORIA, @CONT
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_CATEGORIA <> @L_CATEGORIA_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_CATEGORIA_TMP = @L_CATEGORIA
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_RESULTADO C
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							  B.CO_BALOTARIO = @ID_BALOTARIO AND
							  A.CO_EMPRESA = B.CO_EMPRESA AND
							  A.CO_LOCAL = B.CO_LOCAL AND
							  A.CO_CATEGORIA = @L_CATEGORIA AND
							  C.CO_EMPRESA = B.CO_EMPRESA AND
							  C.CO_BALOTARIO = B.CO_BALOTARIO AND
							  C.CO_USUARIO = A.CO_USUARIO AND
							  C.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_CATEGORIA, @CONT
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos

			END
			
			IF @TIPO_BAL = 'I'  --- Individual
			BEGIN
				DECLARE C_Datos CURSOR FOR
					SELECT a.CO_CATEGORIA , COUNT(*)
					FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_BALOTARIO_PERSONA C
					WHERE B.CO_EMPRESA = @ID_EMPRESA AND
						  B.CO_BALOTARIO = @ID_BALOTARIO AND
						  C.CO_BALOTARIO = B.CO_BALOTARIO AND
						  C.CO_USUARIO = A.CO_USUARIO AND
						  A.CO_EMPRESA = B.CO_EMPRESA 
					GROUP BY A.CO_CATEGORIA
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_CATEGORIA, @CONT
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_CATEGORIA <> @L_CATEGORIA_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_CATEGORIA_TMP = @L_CATEGORIA
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_BALOTARIO_PERSONA C, BAL_RESULTADO D
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							B.CO_BALOTARIO = @ID_BALOTARIO AND
							C.CO_BALOTARIO = B.CO_BALOTARIO AND
							C.CO_USUARIO = A.CO_USUARIO AND
							A.CO_EMPRESA = B.CO_EMPRESA AND
							D.CO_EMPRESA = B.CO_EMPRESA AND
							D.CO_BALOTARIO = B.CO_BALOTARIO AND
							D.CO_USUARIO = A.CO_USUARIO AND
							A.CO_CATEGORIA= @L_CATEGORIA AND
						    D.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_CATEGORIA, @CONT
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos
			END
		END   ---- Fin pestaña Categoría

		IF @ID_TIPOCON = 'A' --- PESTAÑA AREA
		BEGIN
			IF @TIPO_BAL = 'C' -- x cargo
			BEGIN
				DECLARE C_Datos CURSOR FOR
				SELECT a.CO_AREA , COUNT(*)
				FROM BAL_USUARIO A, BAL_BALOTARIO B
				WHERE B.CO_EMPRESA = @ID_EMPRESA AND
					  B.CO_BALOTARIO = @ID_BALOTARIO AND
					  A.CO_EMPRESA = B.CO_EMPRESA AND
					  A.CO_CARGO = B.CO_CARGO
				GROUP BY A.CO_AREA	   	   
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_AREA, @CONT --,  @L_FLAG
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_AREA <> @L_AREA_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_AREA_TMP = @L_AREA
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_RESULTADO C
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							  B.CO_BALOTARIO = @ID_BALOTARIO AND
							  A.CO_EMPRESA = B.CO_EMPRESA AND
							  A.CO_CARGO = B.CO_CARGO AND
							  A.CO_AREA = @L_AREA AND
							  C.CO_EMPRESA = B.CO_EMPRESA AND
							  C.CO_BALOTARIO = B.CO_BALOTARIO AND
							  C.CO_USUARIO = A.CO_USUARIO AND
							  C.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_AREA, @CONT 
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos
			END

			IF @TIPO_BAL = 'L' -- Local
			BEGIN
				DECLARE C_Datos CURSOR FOR
					SELECT a.CO_AREA, COUNT(*)
					FROM BAL_USUARIO A, BAL_BALOTARIO B
					WHERE B.CO_EMPRESA = @ID_EMPRESA AND
						  B.CO_BALOTARIO = @ID_BALOTARIO AND
						  A.CO_EMPRESA = B.CO_EMPRESA AND
						  A.CO_LOCAL = B.CO_LOCAL
					GROUP BY A.CO_AREA	 
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_AREA, @CONT
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_AREA <> @L_AREA_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_AREA_TMP =@L_AREA
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_RESULTADO C
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							  B.CO_BALOTARIO = @ID_BALOTARIO AND
							  A.CO_EMPRESA = B.CO_EMPRESA AND
							  A.CO_LOCAL = B.CO_LOCAL AND
							  A.CO_AREA = @L_AREA AND
							  C.CO_EMPRESA = B.CO_EMPRESA AND
							  C.CO_BALOTARIO = B.CO_BALOTARIO AND
							  C.CO_USUARIO = A.CO_USUARIO AND
							  C.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_AREA, @CONT
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos

			END
			
			IF @TIPO_BAL = 'I'  --- Individual
			BEGIN
				DECLARE C_Datos CURSOR FOR
					SELECT a.CO_AREA , COUNT(*)
					FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_BALOTARIO_PERSONA C
					WHERE B.CO_EMPRESA = @ID_EMPRESA AND
						  B.CO_BALOTARIO = @ID_BALOTARIO AND
						  C.CO_BALOTARIO = B.CO_BALOTARIO AND
						  C.CO_USUARIO = A.CO_USUARIO AND
						  A.CO_EMPRESA = B.CO_EMPRESA 
					GROUP BY A.CO_AREA
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_AREA, @CONT
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_AREA <> @L_AREA_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_AREA_TMP = @L_AREA
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_BALOTARIO_PERSONA C, BAL_RESULTADO D
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							B.CO_BALOTARIO = @ID_BALOTARIO AND
							C.CO_BALOTARIO = B.CO_BALOTARIO AND
							C.CO_USUARIO = A.CO_USUARIO AND
							A.CO_EMPRESA = B.CO_EMPRESA AND
							D.CO_EMPRESA = B.CO_EMPRESA AND
							D.CO_BALOTARIO = B.CO_BALOTARIO AND
							D.CO_USUARIO = A.CO_USUARIO AND
							A.CO_AREA = @L_AREA AND
						    D.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_AREA, @CONT
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos
			END
		END   ---- Fin pestaña Área


		IF @ID_TIPOCON = 'L' --- PESTAÑA LOCAL
		BEGIN
			IF @TIPO_BAL = 'C' -- x cargo
			BEGIN
				DECLARE C_Datos CURSOR FOR
				SELECT a.CO_LOCAL , COUNT(*)
				FROM BAL_USUARIO A, BAL_BALOTARIO B
				WHERE B.CO_EMPRESA = @ID_EMPRESA AND
					  B.CO_BALOTARIO = @ID_BALOTARIO AND
					  A.CO_EMPRESA = B.CO_EMPRESA AND
					  A.CO_CARGO = B.CO_CARGO
				GROUP BY A.CO_LOCAL	   	   
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_LOCAL, @CONT 
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_LOCAL <> @L_LOCAL_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_LOCAL_TMP = @L_LOCAL
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_RESULTADO C
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							  B.CO_BALOTARIO = @ID_BALOTARIO AND
							  A.CO_EMPRESA = B.CO_EMPRESA AND
							  A.CO_CARGO = B.CO_CARGO AND
							  A.CO_LOCAL = @L_LOCAL AND
							  C.CO_EMPRESA = B.CO_EMPRESA AND
							  C.CO_BALOTARIO = B.CO_BALOTARIO AND
							  C.CO_USUARIO = A.CO_USUARIO AND
							  C.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_LOCAL, @CONT 
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos
			END

			IF @TIPO_BAL = 'L' -- Local
			BEGIN
				DECLARE C_Datos CURSOR FOR
					SELECT a.CO_LOCAL, COUNT(*)
					FROM BAL_USUARIO A, BAL_BALOTARIO B
					WHERE B.CO_EMPRESA = @ID_EMPRESA AND
						  B.CO_BALOTARIO = @ID_BALOTARIO AND
						  A.CO_EMPRESA = B.CO_EMPRESA AND
						  A.CO_LOCAL = B.CO_LOCAL
					GROUP BY A.CO_LOCAL	 
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_LOCAL, @CONT
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_LOCAL <> @L_LOCAL_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_LOCAL_TMP = @L_LOCAL
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_RESULTADO C
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							  B.CO_BALOTARIO = @ID_BALOTARIO AND
							  A.CO_EMPRESA = B.CO_EMPRESA AND
							  A.CO_LOCAL = B.CO_LOCAL AND
							  A.CO_LOCAL = @L_LOCAL AND
							  C.CO_EMPRESA = B.CO_EMPRESA AND
							  C.CO_BALOTARIO = B.CO_BALOTARIO AND
							  C.CO_USUARIO = A.CO_USUARIO AND
							  C.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_LOCAL, @CONT
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos

			END
			
			IF @TIPO_BAL = 'I'  --- Individual
			BEGIN
				DECLARE C_Datos CURSOR FOR
					SELECT a.CO_LOCAL , COUNT(*)
					FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_BALOTARIO_PERSONA C
					WHERE B.CO_EMPRESA = @ID_EMPRESA AND
						  B.CO_BALOTARIO = @ID_BALOTARIO AND
						  C.CO_BALOTARIO = B.CO_BALOTARIO AND
						  C.CO_USUARIO = A.CO_USUARIO AND
						  A.CO_EMPRESA = B.CO_EMPRESA 
					GROUP BY A.CO_LOCAL
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_LOCAL, @CONT
				WHILE (@@fetch_status <> -1)
				 Begin	

					IF @L_LOCAL <> @L_LOCAL_TMP
					BEGIN
						SELECT @TOTAL = @TOTAL + 1;
						SET @L_LOCAL_TMP = @L_LOCAL
					END
					
					SELECT @RESUL = COUNT(*) 
					  FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_BALOTARIO_PERSONA C, BAL_RESULTADO D
						WHERE B.CO_EMPRESA = @ID_EMPRESA AND
							B.CO_BALOTARIO = @ID_BALOTARIO AND
							C.CO_BALOTARIO = B.CO_BALOTARIO AND
							C.CO_USUARIO = A.CO_USUARIO AND
							A.CO_EMPRESA = B.CO_EMPRESA AND
							D.CO_EMPRESA = B.CO_EMPRESA AND
							D.CO_BALOTARIO = B.CO_BALOTARIO AND
							D.CO_USUARIO = A.CO_USUARIO AND
							A.CO_LOCAL = @L_LOCAL AND
						    D.QT_INTENTO = 1

					 IF @RESUL = @CONT	
						SELECT @VERDE = @VERDE + 1;
					 ELSE
						SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_LOCAL, @CONT
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos
			END
		END   ---- Fin pestaña Local


		if @TOTAL = 0	SELECT @TOTAL = 1;

		-- Devuelve resultado
		set @POR_VERDE = Round((cast(@VERDE as float)/cast( @TOTAL as float)) * 100,0)
		set @POR_ROJO  = Round((cast(@ROJO as float)/cast( @TOTAL as float)) * 100,0)

		select @POR_VERDE as POR_VERDE, 
				@POR_ROJO as POR_ROJO,
				@TOTAL AS TOTAL,
				@VERDE AS VERDE,
				@ROJO  AS ROJO, 
				FORMAT(@FECHA_FIN, 'dd/MM/yyyy') AS FEC_CIERRE


	END
	ELSE
	BEGIN 
		SELECT @COD_RESULTADO = 0;
		SELECT @MSG_RESULTADO =  'No se encotraron datos!';
	END
END
