
-- EXEC SP_INDICADOR_NOTAS 1, 7
-- SELECT * FROM BAL_BALOTARIO_PERSONA
CREATE PROCEDURE [dbo].[SP_INDICADOR_NOTAS]
	@ID_EMPRESA INT=0,
	@ID_BALOTARIO BIGINT=0,
	@ID_CATEGORIA BIGINT=0,
	@ID_AREA  BIGINT=0,
	@ID_LOCAL BIGINT=0,
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT
AS

DECLARE @COUNT     INT,
		@ROWCOUNT  INT,
		@L_CARGO   INT,
		@L_FLAG    INT,
		@FECHA_FIN DATE,
		@VERDE	   INT=0,
		@TOTAL	   INT=0,
		@ROJO      INT=0,
		@POR_VERDE float,
		@POR_ROJO  float,
		@TIPO_BAL  CHAR(1);
BEGIN

	select no_desctipodocu AS TIPODOCU, 
	co_numdocumento AS NUMDOCU, 
	CONCAT(no_primernombre, ' ', no_segundonombre, ' ', no_apepaterno, ' ', no_apematerno) AS NOMBRES, 
	no_desccargo AS CARGO, no_desccategoria AS CATEGORIA, no_descarea AS AREA, no_desclocal AS LOCAL,
	IsNull((select max(QT_NOTA) from BAL_RESULTADO where
		CO_EMPRESA = @ID_EMPRESA and 
		CO_BALOTARIO = @ID_BALOTARIO and
		CO_USUARIO = b.CO_USUARIO
		),0) as NOTA,
	IsNull((select count(*) from BAL_RESULTADO where
	CO_EMPRESA = @ID_EMPRESA and 
	CO_BALOTARIO = @ID_BALOTARIO and
	CO_USUARIO = b.CO_USUARIO
	),0) as INTENTOS
	 from 
	bal_resultado as a,
	bal_usuario as b,
	bal_cargo as f,
	bal_local as g,
	bal_area as h,
	bal_tipodocu as i,
	bal_categoria as j
	where 
	a.co_usuario = b.co_usuario and 
	b.co_cargo = f.co_cargo and 
	b.co_local = g.co_local and 
	b.co_area = h.co_area and 
	b.co_tipodocumento = i.co_tipodocumento and 
	b.co_categoria = j.co_categoria and 
	b.co_empresa = @ID_EMPRESA  and
	a.co_balotario = @ID_BALOTARIO

END

