USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_GRABAR_LOCAL]    Script Date: 27/09/2020 18:53:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GRABAR_LOCAL]
@Co_Local int,
@Co_Area int,
@Co_Categoria int,
@Desc_Local VARCHAR(200),
@Co_Estado int,
@flagEditar int,
@NEW_IDENTITY  INT = NULL OUTPUT,
@COD_RESULTADO INT OUTPUT,
@MSG_RESULTADO VARCHAR(3000) OUTPUT
AS

SET NOCOUNT ON;
BEGIN TRY

if @flagEditar = 0
BEGIN
DECLARE @CODEMPRESA  INT = 1
DECLARE @CODLOCAL INT = (SELECT MAX(Co_Local)+1 Co_Local
FROM BAL_LOCAL);
INSERT INTO BAL_LOCAL(
Co_Local,
Co_Categoria,
Co_Area,
Co_empresa,
No_DescLocal,
No_DescCortaLocal,
Co_Estado)
VALUES (
@CODLOCAL,
@Co_Categoria,
@Co_Area,
@CODEMPRESA,
@Desc_Local,
'',
'1'
)
SET @NEW_IDENTITY = @CODLOCAL;
SELECT @COD_RESULTADO = 1;
SELECT @MSG_RESULTADO = 'OK';
END

ELSE
BEGIN

UPDATE  BAL_LOCAL
SET
Co_Categoria = @Co_Categoria,
Co_Area = @Co_Area,
No_DescLocal = @Desc_Local,
Co_Estado = @Co_Estado
WHERE Co_Local = @Co_Local

SELECT @COD_RESULTADO = 1;
SELECT @MSG_RESULTADO = 'OK';
END;
END TRY
BEGIN CATCH

THROW;

END CATCH


