USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_VALIDAR_BALOTARIO_VIGENTE]    Script Date: 02/07/2020 22:14:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Joel Reymundez
-- Create date: 28/06/2020
-- Description:	Valida si existe un balotario vigente con el mismo tipo 
-- Modificado date: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_VALIDAR_BALOTARIO_VIGENTE]
	@FECHA_INICIO VARCHAR(20),
	@FECHA_FIN VARCHAR(20),
	@TIPO_BALOTARIO VARCHAR (20),

	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT
AS

DECLARE @GLOBAL_COD_EMPRESA INT = 1;
DECLARE @COUNT INT;
BEGIN

		SELECT Fe_Inicio, Fe_Fin ,Fi_TipoBal
		FROM BAL_BALOTARIO 
		WHERE BAL_BALOTARIO.CO_EMPRESA = @GLOBAL_COD_EMPRESA
		AND BAL_BALOTARIO.Fe_Inicio = @FECHA_INICIO
		AND BAL_BALOTARIO.Fe_Fin = @FECHA_FIN
		AND BAL_BALOTARIO.Fi_TipoBal = @TIPO_BALOTARIO


		SELECT @COUNT = @@ROWCOUNT
			
		IF @COUNT > 0
		BEGIN
			SELECT @COD_RESULTADO = 1;
			SELECT @MSG_RESULTADO = 'OK';
		END
		ELSE
		BEGIN 
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO =  'No se encotraron datos!';
		END
END

