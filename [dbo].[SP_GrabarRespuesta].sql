USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_GrabarRespuesta]    Script Date: 30/08/2020 17:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GrabarRespuesta]
@Co_empresa int,
@Co_Pregunta int,
@Co_Respuesta int,
@No_DescRespuesta   VARCHAR(200),
@No_DescCortaRespuesta VARCHAR(30),
@Fi_Correcta char(1),
@flagEditar int,
@COD_RESULTADO INT OUTPUT,
@MSG_RESULTADO VARCHAR(3000) OUTPUT
AS
SET NOCOUNT ON;

if @flagEditar = 0 OR @flagEditar = 1
BEGIN
INSERT INTO BAL_RESPUESTA
(
Co_empresa,
Co_Pregunta,
Co_Respuesta,
No_DescRespuesta,
No_DescCortaRespuesta,
Fi_Correcta
)
VALUES
( 
@Co_empresa ,
@Co_Pregunta,
@Co_Respuesta,
@No_DescRespuesta,
@No_DescCortaRespuesta ,
@Fi_Correcta
)

SELECT @COD_RESULTADO = 1;
SELECT @MSG_RESULTADO = 'OK';
END;