
-- EXEC SP_INDICADOR_MONITOR 1, 1
-- SELECT * FROM BAL_BALOTARIO_PERSONA
CREATE PROCEDURE [dbo].[SP_INDICADOR_MONITOR]
	@ID_EMPRESA INT=0,
	@ID_BALOTARIO BIGINT=0,
	@ID_TIPOCON CHAR(1)='C',
	@ID_CATEGORIA BIGINT=0,
	@ID_AREA  BIGINT=0,
	@ID_LOCAL BIGINT=0,
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT
AS

DECLARE @COUNT     INT,
		@ROWCOUNT  INT,
		@L_CARGO   INT,
		@L_FLAG    INT,
		@FECHA_FIN DATE,
		@VERDE	   INT=0,
		@TOTAL	   INT=0,
		@ROJO      INT=0,
		@POR_VERDE float,
		@POR_ROJO  float,
		@TIPO_BAL  CHAR(1);
BEGIN

	-- Tipo de evaluación	
	SELECT @TIPO_BAL = FI_TIPOBAL,
		   @FECHA_FIN = FE_FIN	 
	FROM BAL_BALOTARIO 
	WHERE CO_EMPRESA = @ID_EMPRESA
	AND   Co_Balotario = @ID_BALOTARIO

	SELECT @ROWCOUNT = @@ROWCOUNT
	IF(@ROWCOUNT > 0)
	BEGIN
		SELECT @COD_RESULTADO = 1;
		SELECT @MSG_RESULTADO = 'OK';

		IF @ID_TIPOCON = 'C' --- PESTAÑA CARGO
		BEGIN
			IF @TIPO_BAL = 'C'
			BEGIN
				DECLARE C_Datos CURSOR FOR
				SELECT a.CO_CARGO, 
					   ( SELECT COUNT(*) FROM BAL_RESULTADO
					   WHERE CO_EMPRESA = @ID_EMPRESA AND
							 CO_USUARIO = A.CO_USUARIO AND
							 CO_BALOTARIO = B.CO_BALOTARIO AND
							 QT_INTENTO = 1 )  AS FLAG 
 
				FROM BAL_USUARIO A, BAL_BALOTARIO B
				WHERE B.CO_EMPRESA = @ID_EMPRESA AND
					  B.CO_BALOTARIO = @ID_BALOTARIO AND
					  A.CO_EMPRESA = B.CO_EMPRESA AND
					  A.CO_CARGO = B.CO_CARGO 
					  --A.CO_ESTADO = '1'
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_CARGO,  @L_FLAG
				WHILE (@@fetch_status <> -1)
				 Begin	

				 SELECT @TOTAL = @TOTAL + 1;
			 
				 IF @L_FLAG = 1	SELECT @VERDE = @VERDE + 1;

				 IF @L_FLAG = 0 SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_CARGO,  @L_FLAG
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos
			END

			IF @TIPO_BAL = 'L'
			BEGIN
				DECLARE C_Datos CURSOR FOR
				SELECT a.CO_CARGO, 
					   ( SELECT COUNT(*) FROM BAL_RESULTADO
					   WHERE CO_EMPRESA = @ID_EMPRESA AND
							 CO_USUARIO = A.CO_USUARIO AND
							 CO_BALOTARIO = B.CO_BALOTARIO AND
							 QT_INTENTO = 1 )  AS FLAG 
 
				FROM BAL_USUARIO A, BAL_BALOTARIO B
				WHERE B.CO_EMPRESA = @ID_EMPRESA AND
					  B.CO_BALOTARIO = @ID_BALOTARIO AND
					  A.CO_EMPRESA = B.CO_EMPRESA AND
					  A.CO_LOCAL = B.CO_LOCAL
					  --A.CO_ESTADO = '1'
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_CARGO,  @L_FLAG
				WHILE (@@fetch_status <> -1)
				 Begin	

				 SELECT @TOTAL = @TOTAL + 1;
			 
				 IF @L_FLAG = 1	SELECT @VERDE = @VERDE + 1;

				 IF @L_FLAG = 0 SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_CARGO,  @L_FLAG
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos

			END

			IF @TIPO_BAL = 'I'
			BEGIN
				DECLARE C_Datos CURSOR FOR
					SELECT a.co_CARGO ,
						( SELECT COUNT(*) FROM BAL_RESULTADO
					   WHERE CO_EMPRESA = @ID_EMPRESA AND
							 CO_USUARIO = A.CO_USUARIO AND
							 CO_BALOTARIO = B.CO_BALOTARIO AND
							 QT_INTENTO = 1 )  AS FLAG 
				FROM BAL_USUARIO A, BAL_BALOTARIO B, BAL_BALOTARIO_PERSONA C
				WHERE B.CO_EMPRESA = @ID_EMPRESA AND
					  B.CO_BALOTARIO = @ID_BALOTARIO AND
					  C.CO_BALOTARIO = B.CO_BALOTARIO AND
					  C.CO_USUARIO = A.CO_USUARIO AND
					  A.CO_EMPRESA = B.CO_EMPRESA 
					 -- A.CO_ESTADO = '1'
				OPEN C_Datos
				FETCH NEXT FROM C_Datos INTO	@L_CARGO,  @L_FLAG
				WHILE (@@fetch_status <> -1)
				 Begin	

				 SELECT @TOTAL = @TOTAL + 1;
			 
				 IF @L_FLAG = 1	SELECT @VERDE = @VERDE + 1;

				 IF @L_FLAG = 0 SELECT @ROJO = @ROJO + 1;

				 FETCH NEXT FROM C_Datos INTO @L_CARGO,  @L_FLAG
				 END  /*-- While --*/

				CLOSE C_Datos
				DEALLOCATE C_Datos
			END
		END

		if @TOTAL = 0	SELECT @TOTAL = 1;

		-- Devuelve resultado
		set @POR_VERDE = Round((cast(@VERDE as float)/cast( @TOTAL as float)) * 100,0)
		set @POR_ROJO  = Round((cast(@ROJO as float)/cast( @TOTAL as float)) * 100,0)

		select @POR_VERDE as POR_VERDE, 
				@POR_ROJO as POR_ROJO,
				@TOTAL AS TOTAL,
				@VERDE AS VERDE,
				@ROJO  AS ROJO, 
				FORMAT(@FECHA_FIN, 'dd/MM/yyyy') AS FEC_CIERRE


	END
	ELSE
	BEGIN 
		SELECT @COD_RESULTADO = 0;
		SELECT @MSG_RESULTADO =  'No se encotraron datos!';
	END
END




