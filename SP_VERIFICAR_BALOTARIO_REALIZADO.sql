USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_VERIFICAR_BALOTARIO_REALZIADO]    Script Date: 16/07/2020 20:34:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ronald Roca
-- Create date: 16/07/2020
-- Description:	Verificar  si el balotario ya ha sido realziado
-- Modificado date: 
-- =============================================
CREATE PROCEDURE [dbo].[SP_VERIFICAR_BALOTARIO_REALZIADO]
	
	@ID_BALOTARIO BIGINT=0,
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT
	
AS

DECLARE @ROWCOUNT INT;

		SELECT  BAL_RESULTADO.CO_USUARIO FROM BAL_RESULTADO, BAL_BALOTARIO
		WHERE BAL_RESULTADO.CO_BALOTARIO = @ID_BALOTARIO
	 
		SELECT @ROWCOUNT = @@ROWCOUNT
		IF(@ROWCOUNT > 0)
		BEGIN
			SELECT @COD_RESULTADO = 1;
			SELECT @MSG_RESULTADO = 'OK';
		END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = -1;
			SELECT @MSG_RESULTADO = 'NO';
		END

