USE [BD_BALOTARIO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joel Reymundez
-- Description:	Obtener QtPreguntas y QtTemas de la empresa
-- Fecha: 09/07/2020
-- =============================================

CREATE PROCEDURE [dbo].[SP_OBTENER_QT_P_T]
	@COD_EMPRESA INT,
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT

AS
	
	BEGIN TRANSACTION
	BEGIN TRY

SELECT Co_empresa, Qt_Temas,
Qt_Preguntas
FROM BAL_EMPRESA

		COMMIT TRANSACTION
		
		SELECT @COD_RESULTADO = 1;
		SELECT @MSG_RESULTADO = 'OK';

		
	END TRY
	BEGIN CATCH
		
		ROLLBACK TRANSACTION

		SELECT @COD_RESULTADO = 0;
		SELECT @MSG_RESULTADO = ERROR_MESSAGE();

	END CATCH


