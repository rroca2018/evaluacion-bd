USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_INDICADOR_DETALLE]    Script Date: 21/08/2020 15:30:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC SP_INDICADOR_DETALLE 1, 1
-- SELECT * FROM BAL_BALOTARIO_PERSONA
ALTER PROCEDURE [dbo].[SP_INDICADOR_DETALLE]
	@ID_EMPRESA INT=0,
	@ID_BALOTARIO BIGINT=0,
	@ID_CATEGORIA BIGINT=0,
	@ID_AREA  BIGINT=0,
	@ID_LOCAL BIGINT=0,
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT
AS

DECLARE @COUNT     INT,
		@ROWCOUNT  INT,
		@L_CARGO   INT,
		@L_FLAG    INT,
		@FECHA_FIN DATE,
		@VERDE	   INT=0,
		@TOTAL	   INT=0,
		@ROJO      INT=0,
		@POR_VERDE float,
		@POR_ROJO  float,
		@TIPO_BAL  CHAR(1);
BEGIN

	SELECT no_desctipodocu AS TIPODOCU, 
	co_numdocumento AS NUMDOCU, 
	CONCAT(no_primernombre, ' ', no_segundonombre, ' ', no_apepaterno, ' ', no_apematerno) AS NOMBRES, 
	no_desccargo AS CARGO, 
	no_desccategoria AS CATEGORIA, 
	no_descarea AS AREA, 
	no_desclocal AS LOCAL,
	no_desctema AS TEMA, 
	a.Qt_Intento AS INTENTOS,
	no_descpregunta AS PREGUNTA, 
	no_descrespuesta AS RESPUESTA, 
	a.fi_correcta AS CORRE_INCORRE
	FROM 
	bal_resultado_detalle as a,
	bal_usuario as b,
	bal_pregunta as c,
	bal_respuesta as d,
	bal_tema as e,
	bal_cargo as f,
	bal_local as g,
	bal_area as h,
	bal_tipodocu as i,
	bal_categoria as j
	WHERE 
	a.co_usuario = b.co_usuario and 
	a.co_pregunta = c.co_pregunta and 
	c.co_pregunta = d.co_pregunta and 
	a.co_respuesta = d.co_respuesta and 
	c.co_tema = e.co_tema and 
	b.co_cargo = f.co_cargo and 
	b.co_local = g.co_local and 
	b.co_area = h.co_area and 
	b.co_categoria = j.co_categoria and 
	b.co_tipodocumento = i.co_tipodocumento and 
	b.co_empresa = @ID_EMPRESA  and
	a.co_balotario = @ID_BALOTARIO
	SELECT @rowcount = @@ROWCOUNT

		IF @rowcount > 0
			BEGIN
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';
			END
		ELSE
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'No hay data';
		END
END

