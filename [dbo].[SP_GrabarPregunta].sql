USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_GrabarPregunta]    Script Date: 30/08/2020 16:59:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GrabarPregunta]
@Co_Pregunta int,
@Desc_Pregunta VARCHAR(200),
@Desc_CortaPregunta VARCHAR(30),
@Co_Tema int,
@Co_Estado int,
@flagEditar int,
@NEW_IDENTITY  INT = NULL OUTPUT,
@COD_RESULTADO INT OUTPUT,
@MSG_RESULTADO VARCHAR(3000) OUTPUT
AS

SET NOCOUNT ON;
BEGIN TRY

if @flagEditar = 0
BEGIN
DECLARE @CODPREGUNTA INT = (SELECT MAX(Co_Pregunta)+1 Co_Pregunta
FROM BAL_PREGUNTA);
INSERT INTO BAL_PREGUNTA(
Co_empresa,
Co_Pregunta,
No_DescPregunta,
No_DescCortaPregunta,
Co_Tema,
Co_Tipo_Pregunta,
Co_Estado)
VALUES (
'1',
@CODPREGUNTA,
@Desc_Pregunta,
@Desc_CortaPregunta,
@Co_Tema,
'1',
'1'
)
SET @NEW_IDENTITY = @CODPREGUNTA;
SELECT @COD_RESULTADO = 1;
SELECT @MSG_RESULTADO = 'OK';
END
ELSE

BEGIN

UPDATE  BAL_PREGUNTA
SET
No_DescPregunta = @Desc_Pregunta,
No_DescCortaPregunta = @Desc_CortaPregunta,
Co_Tema = @Co_Tema,
Co_Estado = @Co_Estado
WHERE Co_Pregunta = @Co_Pregunta

SELECT @COD_RESULTADO = 1;
SELECT @MSG_RESULTADO = 'OK';
END;
END TRY
BEGIN CATCH

THROW;

END CATCH


