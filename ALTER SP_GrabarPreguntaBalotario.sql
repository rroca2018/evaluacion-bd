USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_GrabarPreguntaBalotario]    Script Date: 15/07/2020 21:40:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ronald Roca
-- Description:	Crear o actualizar en tabla BAL_BALOTARIO_PREGUNTA
-- =============================================
ALTER PROCEDURE [dbo].[SP_GrabarPreguntaBalotario]
	@Co_Balotario			int,
	@Co_Tema				int,
	@Cant_x_tema			int,
	@flagEditar				int,
	@COD_RESULTADO			INT OUTPUT,
	@MSG_RESULTADO			VARCHAR(3000) OUTPUT
AS
DECLARE
@sql varchar(max)

SET NOCOUNT ON;

	if @flagEditar = 0 OR @flagEditar = 1
	BEGIN
		if @Cant_x_tema <= 0
		BEGIN
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO = 'Cantidad no puede ser igual o menor a cero';
		END
		ELSE
		BEGIN
			SET @sql ='INSERT INTO BAL_BALOTARIO_PREGUNTA ' + 
					'SELECT TOP ' + cast( @Cant_x_tema as varchar(10)) + ' ' +
					cast( @Co_Balotario as varchar(10)) + ', CO_PREGUNTA ' +
					'FROM BAL_PREGUNTA WHERE CO_TEMA = ' + 
					cast( @Co_Tema as varchar(10)) + ' ORDER BY NEWID()'

			EXEC (@SQL)

			SELECT @COD_RESULTADO = 1;
			SELECT @MSG_RESULTADO = 'OK';	
		END
	END

--*****
/*
			if @Cant_x_tema = 1
			BEGIN
				INSERT INTO BAL_BALOTARIO_PREGUNTA
				SELECT TOP 1 @Co_Balotario, CO_PREGUNTA 
				FROM BAL_PREGUNTA
				WHERE CO_TEMA = @Co_Tema
				ORDER BY NEWID()
	
				SELECT @COD_RESULTADO = 1;
				SELECT @MSG_RESULTADO = 'OK';			
			END
			ELSE 
			BEGIN
				IF @Cant_x_tema = 2
				BEGIN
					INSERT INTO BAL_BALOTARIO_PREGUNTA
					SELECT TOP 2 @Co_Balotario, CO_PREGUNTA 
					FROM BAL_PREGUNTA
					WHERE CO_TEMA = @Co_Tema
					ORDER BY NEWID()
	
					SELECT @COD_RESULTADO = 1;
					SELECT @MSG_RESULTADO = 'OK';	

				END
				ELSE
				BEGIN

					IF @Cant_x_tema = 3
					BEGIN

						INSERT INTO BAL_BALOTARIO_PREGUNTA
						SELECT TOP 3 @Co_Balotario, CO_PREGUNTA 
						FROM BAL_PREGUNTA
						WHERE CO_TEMA = @Co_Tema
						ORDER BY NEWID()
	
						SELECT @COD_RESULTADO = 1;
						SELECT @MSG_RESULTADO = 'OK';	

					END
					ELSE
					BEGIN
						IF @Cant_x_tema = 4
						BEGIN

							INSERT INTO BAL_BALOTARIO_PREGUNTA
							SELECT TOP 4 @Co_Balotario, CO_PREGUNTA 
							FROM BAL_PREGUNTA
							WHERE CO_TEMA = @Co_Tema
							ORDER BY NEWID()
	
							SELECT @COD_RESULTADO = 1;
							SELECT @MSG_RESULTADO = 'OK';	

						END
						ELSE
						BEGIN
							IF @Cant_x_tema = 5
							BEGIN

								INSERT INTO BAL_BALOTARIO_PREGUNTA
								SELECT TOP 5 @Co_Balotario, CO_PREGUNTA 
								FROM BAL_PREGUNTA
								WHERE CO_TEMA = @Co_Tema
								ORDER BY NEWID()
	
								SELECT @COD_RESULTADO = 1;
								SELECT @MSG_RESULTADO = 'OK';	

							END
							ELSE
							BEGIN
								IF @Cant_x_tema = 6
								BEGIN

									INSERT INTO BAL_BALOTARIO_PREGUNTA
									SELECT TOP 6 @Co_Balotario, CO_PREGUNTA 
									FROM BAL_PREGUNTA
									WHERE CO_TEMA = @Co_Tema
									ORDER BY NEWID()
	
									SELECT @COD_RESULTADO = 1;
									SELECT @MSG_RESULTADO = 'OK';	

								END
								ELSE
								BEGIN
									IF @Cant_x_tema = 7
									BEGIN

										INSERT INTO BAL_BALOTARIO_PREGUNTA
										SELECT TOP 7 @Co_Balotario, CO_PREGUNTA 
										FROM BAL_PREGUNTA
										WHERE CO_TEMA = @Co_Tema
										ORDER BY NEWID()
	
										SELECT @COD_RESULTADO = 1;
										SELECT @MSG_RESULTADO = 'OK';	

									END
									ELSE
									BEGIN

										IF @Cant_x_tema = 8
										BEGIN

											INSERT INTO BAL_BALOTARIO_PREGUNTA
											SELECT TOP 8 @Co_Balotario, CO_PREGUNTA 
											FROM BAL_PREGUNTA
											WHERE CO_TEMA = @Co_Tema
											ORDER BY NEWID()
	
											SELECT @COD_RESULTADO = 1;
											SELECT @MSG_RESULTADO = 'OK';	

										END
										ELSE
										BEGIN

											IF @Cant_x_tema = 9
											BEGIN

												INSERT INTO BAL_BALOTARIO_PREGUNTA
												SELECT TOP 9 @Co_Balotario, CO_PREGUNTA 
												FROM BAL_PREGUNTA
												WHERE CO_TEMA = @Co_Tema
												ORDER BY NEWID()
	
												SELECT @COD_RESULTADO = 1;
												SELECT @MSG_RESULTADO = 'OK';	

											END
											ELSE
											BEGIN
												SELECT @COD_RESULTADO = 0;
												SELECT @MSG_RESULTADO = 'Cantidad no puede ser mayor a 10';

											END;

										END;


									END;

								END;


							END;

						END;

					END;

				END;
			END;
				
		END;

	END
*/



