USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_ELIMINAR_PREGUNTA_BALOTARIO]    Script Date: 15/07/2020 21:38:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ronald Roca
-- Description:	Crear o actualizar en tabla BAL_BALOTARIO_PREGUNTA
-- =============================================
CREATE  PROCEDURE [dbo].[SP_ELIMINAR_PREGUNTA_BALOTARIO]
	@Co_Balotario			int,
	@COD_RESULTADO			INT OUTPUT,
	@MSG_RESULTADO			VARCHAR(3000) OUTPUT
AS

	BEGIN	
--		Elimina registros.
		DELETE FROM BAL_BALOTARIO_PREGUNTA
		WHERE Co_Balotario = @Co_Balotario;
		SELECT @COD_RESULTADO = 1;
		SELECT @MSG_RESULTADO = 'OK';	
    END;

	