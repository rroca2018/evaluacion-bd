USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_OBTENER_BALOTARIOS_COMPLETO]    Script Date: 23/07/2020 20:00:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SP_OBTENER_BALOTARIOS_COMPLETO]
	@ID_BALOTARIO BIGINT=0,
	
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT
AS

DECLARE @GLOBAL_COD_EMPRESA INT = 1;
DECLARE @COUNT INT;
DECLARE @BALOTARIO_REALIZADO INT;
BEGIN
			BEGIN		
			SELECT @BALOTARIO_REALIZADO = [dbo].[FN_BAL_REALIZADO] (@ID_BALOTARIO);
			END
			SELECT B.Co_Balotario, B.Fe_Inicio, B.Fe_Fin, B.No_Descripcion, 
			B.Qt_Intentos, B.Qt_Tiempo,
			B.Qt_CaliAprob, B.No_Mensaje, CASE B.FI_TIPOBAL WHEN 'C' THEN 'CARGO' WHEN 'L' 
			THEN 'LOCAL' WHEN 'I' THEN 'PERSONAS' END AS TIPO, 
			B.Fi_TipoBal,
			B.Co_Cargo, B.Co_Categoria, B.Co_Area , B.Co_Local,
			C.No_DescCargo, CAT.No_DescCategoria, A.No_DescArea, L.No_DescLocal,@BALOTARIO_REALIZADO AS BALOTARIO_REALIZADO
			FROM BAL_BALOTARIO B 
			LEFT JOIN BAL_CARGO C ON B.Co_Cargo = C.Co_Cargo AND B.Co_empresa = C.Co_empresa
			LEFT JOIN BAL_CATEGORIA CAT ON B.Co_Categoria = CAT.Co_Categoria AND B.Co_empresa = CAT.Co_empresa
			LEFT JOIN BAL_AREA A ON B.Co_Area = A.Co_Area AND B.Co_empresa = A.Co_empresa
			LEFT JOIN BAL_LOCAL L ON B.Co_Local = L.Co_Local AND B.Co_empresa = L.Co_empresa
		WHERE  B.Co_empresa = @GLOBAL_COD_EMPRESA
		AND B.Co_Balotario = @ID_BALOTARIO


		SELECT @COUNT = @@ROWCOUNT
			
		IF @COUNT > 0
		BEGIN
			SELECT @COD_RESULTADO = 1;
			SELECT @MSG_RESULTADO = 'OK';
		END
		ELSE
		BEGIN 
			SELECT @COD_RESULTADO = 0;
			SELECT @MSG_RESULTADO =  'No se encotraron datos!';
		END
END

