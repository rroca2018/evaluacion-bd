USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_BALOTARIO_CANTIDAD_PREGUNTAS]    Script Date: 09/07/2020 21:32:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joel Reymundez
-- Description:	Obtener la cantidad de preguntas por tema de un balotario
-- Fecha: 07/07/2020
-- =============================================

ALTER PROCEDURE [dbo].[SP_BALOTARIO_CANTIDAD_PREGUNTAS]
	@COD_BALOTARIO INT,
	@COD_RESULTADO INT=0 OUT,
	@MSG_RESULTADO varchar(100)='' OUT

AS
	
	BEGIN TRANSACTION
	BEGIN TRY

SELECT	--ROW_NUMBER() as correlativo, 
		c.Co_Tema ,	ROW_NUMBER()
		OVER(ORDER BY C.No_DescTema ASC) AS correlativo,
		C.No_DescTema,COUNT (b.No_DescPregunta) AS CANTIDAD
		FROM BAL_BALOTARIO_PREGUNTA as A, BAL_PREGUNTA as B, BAL_TEMA as C
		WHERE A.Co_Balotario = @COD_BALOTARIO AND
			  A.Co_Pregunta  = B.Co_Pregunta  AND
			  B.Co_Tema      = C.Co_Tema
		--GROUP BY C.No_DescTema, b.No_DescPregunta
		GROUP BY 	C.No_DescTema , C.Co_Tema

		COMMIT TRANSACTION
		
		SELECT @COD_RESULTADO = 1;
		SELECT @MSG_RESULTADO = 'OK';

		
	END TRY
	BEGIN CATCH
		
		ROLLBACK TRANSACTION

		SELECT @COD_RESULTADO = 0;
		SELECT @MSG_RESULTADO = ERROR_MESSAGE();

	END CATCH


