USE [BD_BALOTARIO]
GO
/****** Object:  StoredProcedure [dbo].[SP_GRABAR_AREA]    Script Date: 27/09/2020 18:52:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GRABAR_AREA]
@Co_Area int,
@Co_Categoria int,
@Desc_Area VARCHAR(200),
@Co_Estado int,
@flagEditar int,
@NEW_IDENTITY  INT = NULL OUTPUT,
@COD_RESULTADO INT OUTPUT,
@MSG_RESULTADO VARCHAR(3000) OUTPUT
AS

SET NOCOUNT ON;
BEGIN TRY

if @flagEditar = 0
BEGIN
DECLARE @CODEMPRESA  INT = 1
DECLARE @CODAREA INT = (SELECT MAX(Co_Area)+1 Co_Area
FROM BAL_AREA);
INSERT INTO BAL_AREA(
Co_Categoria,
Co_Area,
Co_empresa,
No_DescArea,
No_DescCortaArea,
Co_Estado)
VALUES (
@Co_Categoria,
@CODAREA,
@CODEMPRESA,
@Desc_Area,
'',
'1'
)
SET @NEW_IDENTITY = @CODAREA;
SELECT @COD_RESULTADO = 1;
SELECT @MSG_RESULTADO = 'OK';
END

ELSE
BEGIN

UPDATE  BAL_AREA
SET
Co_Categoria = @Co_Categoria,
No_DescArea = @Desc_Area,
Co_Estado = @Co_Estado
WHERE Co_Area = @Co_Area

SELECT @COD_RESULTADO = 1;
SELECT @MSG_RESULTADO = 'OK';
END;
END TRY
BEGIN CATCH

THROW;

END CATCH


